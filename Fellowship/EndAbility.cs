﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace Fellowship
{
    class EndAbility: LimitedAbility
    {
        public EndAbility(Game game)
        {
            NUMBER_OF_USES = 1;
            icon = game.Content.Load<Texture2D>(@"Icons/3");
        }

        public override void Launch(Adventurer adv)
        {
            if (available())
            {
                NUMBER_OF_USES--;
                adv.isDead = true;
                adv.level.numberOfAdventurers--;
            }
           
        }
    }
}
