﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Fellowship
{
    public interface Ability
    {
        bool available(); 

        void Launch(Adventurer adv);
        void Update(GameTime gameTime);

        Texture2D icon
        {
            get;
            set;
        }

    }
}
