﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Fellowship
{
    class AdventurerUI : Microsoft.Xna.Framework.DrawableGameComponent
    {
        internal GraphicsDeviceManager graphics;

        Rectangle startingCoor;

        List<Adventurer> adventurers;
        List<Rectangle> iconAreas = new List<Rectangle>();

        Level level;

        MouseState currentMouseState;
        MouseState previousMouseState;

        public AdventurerUI(Game1 game)
            : base(game)
        {
            level = game.modelManager;
            adventurers = level.adventurers;

            graphics = game.graphics;
            
            startingCoor = new Rectangle(
                graphics.PreferredBackBufferWidth/2 - 200, 
                graphics.PreferredBackBufferHeight - 110, 
                70, 100);
        }

        public override void Initialize()
        {
            for (int i = 0; i < adventurers.Count; i++)
            {
                iconAreas.Add(new Rectangle(
                    startingCoor.X + 100 * i, startingCoor.Y,
                    startingCoor.Width, startingCoor.Height));
            }

            base.Initialize();
        }

        protected override void LoadContent()
        {
        }

        public override void Update(GameTime gameTime)
        {
            HandleMouse();

            base.Update(gameTime);
        }

        private void HandleMouse()
        {
            
            currentMouseState = Mouse.GetState();

            for (int i = 0; i < iconAreas.Count; i++)
            {
                Rectangle rect = iconAreas.ElementAt(i);

                if (rect.Contains(new Point(previousMouseState.X, previousMouseState.Y))
                    && previousMouseState.LeftButton == ButtonState.Pressed && !adventurers.ElementAt(i).isDead)
                {
                    if (currentMouseState.LeftButton == ButtonState.Released)
                    {
                        foreach (Adventurer adv in level.adventurers)
                            adv.isSelected = false;
                        level.activeAdventurer = adventurers.ElementAt(i);
                        level.activeAdventurer.isSelected = true;
                    }
                }
            }
            previousMouseState = Mouse.GetState();
        }



        public override void Draw(GameTime gameTime)
        {

            ((Game1)Game).spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, RasterizerState.CullNone);
            for (int i = 0; i < adventurers.Count; i++)
            {

                if (!adventurers.ElementAt(i).isDead)
                {
                    if (adventurers.ElementAt(i).isSelected)
                    {
                        Texture2D boundary = new Texture2D(graphics.GraphicsDevice, 80, 110);

                        Color[] data = new Color[80 * 110];
                        for (int j = 0; j < data.Length; ++j) data[j] = Color.Chocolate;
                        boundary.SetData(data);

                        Vector2 coor = new Vector2(startingCoor.X - 5 + 100 * i, startingCoor.Y - 5);
                        ((Game1)Game).spriteBatch.Draw(boundary, coor, Color.White);
                    }

                    ((Game1)Game).spriteBatch.Draw(adventurers.ElementAt(i).icon, iconAreas.ElementAt(i), Color.White);
                }
            }
            ((Game1)Game).spriteBatch.End();
            base.Draw(gameTime);
        }

    }
}
