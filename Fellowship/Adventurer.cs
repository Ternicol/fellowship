﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Fellowship
{
    public class Adventurer
    {
        public Level level;

        internal CustomModel model;

        public List<Ability> abilities = new List<Ability>();

        public readonly float GRAVITY = 0.7f;

        protected Vector3 position;
        public Vector3 size;
        public Vector3 speed { get; set; }
        public Vector2 acceleration;
        public Vector3 nominalSpeed = new Vector3(20.0f, 0.0f, 0.0f);
        public Vector3 nominalSpeedPositive = new Vector3(50, 50, 50);
        public Vector3 nominalSpeedNegative = new Vector3(-50, -50, -50);

        public bool isFalling = false;
        public bool isJumping = false;

        public bool isDead = false;
        public bool reachedEnd = false;
        public String info = "";

        public bool lavaAbility, lavaAbilityUsed; 
        LavaBlock lavaBlock; 

        protected bool collision15 = false;
        protected bool collision04 = false;
        protected bool collision26 = false;
        protected bool collision37 = false;

        protected bool collisionWithLava = false;
        protected bool collisionWithGrass = false;
        protected bool collisionWithWater = false;
        protected bool collisionWithStone = false;

        protected SoundEffect footstepEffectGrass;
        public SoundEffectInstance footstepEffectGrassInstance;
        protected SoundEffect swimmingEffectWater;
        public SoundEffectInstance swimmingEffectWaterInstance;
        protected SoundEffect footstepEffectStone;
        public SoundEffectInstance footstepEffectStoneInstance;
        protected SoundEffect impactEffectLava;
        protected SoundEffectInstance impactEffectLavaInstance;

        protected Vector2 endAreaCoor;
        protected Vector2 lavaAreaCoor;

        protected float distanceToLava;
        protected float distanceToCampfire;

        public Texture2D icon;
        public bool isSelected = false;
        public bool readyToSpawn = false;

        public bool facingRight = true;

        public Adventurer(Level level, CustomModel m)
        {
            this.level = level;
            model = m;

            position = Vector3.Zero;
            size = Vector3.One;
            speed = Vector3.Zero;
            lavaAbility = false;
            lavaAbilityUsed = false; 

        }
        public Adventurer(Level level, CustomModel m, Vector3 position, Vector3 size)
        {
            this.level = level;
            model = m;

            this.position = position;
            this.size = size;
            speed = Vector3.Zero;

            model.scale(Matrix.CreateScale(size.X, size.Y, size.Z));
            model.translate(Matrix.CreateTranslation(position.X, position.Y, position.Z));
            model.orientRotateY(MathHelper.PiOver2);

            endAreaCoor = new Vector2(level.endArea.area.X, level.endArea.area.Y);
            lavaAreaCoor = FindLavaCoor(level.groundBlocks);

            lavaAbility = false; 

            LoadSoundEffects();
        }


        public void LoadSoundEffects()
        {
            footstepEffectGrass = level.game.Content.Load<SoundEffect>(@"Sounds/footsteps_grass_1");
            footstepEffectGrassInstance = footstepEffectGrass.CreateInstance();
            footstepEffectGrassInstance.IsLooped = true;
            footstepEffectGrassInstance.Volume = 0.5f;

            footstepEffectStone = level.game.Content.Load<SoundEffect>(@"Sounds/footsteps_stone_1");
            footstepEffectStoneInstance = footstepEffectStone.CreateInstance();
            footstepEffectStoneInstance.IsLooped = true;
            footstepEffectStoneInstance.Volume = 0.3f;

            swimmingEffectWater = level.game.Content.Load<SoundEffect>(@"Sounds/water");
            swimmingEffectWaterInstance = swimmingEffectWater.CreateInstance();
            swimmingEffectWaterInstance.IsLooped = true;
            swimmingEffectWaterInstance.Volume = 0.1f;

            impactEffectLava = level.game.Content.Load<SoundEffect>(@"Sounds/Explosion Small Muffled");
            impactEffectLavaInstance = impactEffectLava.CreateInstance();
            impactEffectLavaInstance.Volume = 0.5f;
        }

        public void move(float x, float y, float z)
        {
            model.translate(Matrix.CreateTranslation(x, y, z));
        }
        public void Update(List<TerrainBlock> l)
        {
            if (isDead)
                info = "Adventurer died";
            else
                info = "";

            CheckCollision(l);
            HandleCollision();
            HandleKeyboardInput();

            ComputeDistances(lavaAreaCoor, endAreaCoor);
            HandleAdventurerState();

            move(nominalSpeed.X, nominalSpeed.Y, 0);
            Fall();

            model.Update();
        }

        public void Draw(Camera camera)
        {
            model.Draw(camera);
        }

       virtual public BoundingBox getBoundingBox()
        {
            return model.getBoundingBox();
        }

        public Vector3 getPosition()
        {
            return model.getPosition();
        }

        private void CheckCollision(List<TerrainBlock> groundBlocks)
        {
            Vector3[] corners = getBoundingBox().GetCorners();

            foreach (TerrainBlock m in groundBlocks)
            {

                if (m.getBoundingBox().Contains(corners[1]) == ContainmentType.Contains
                    && m.getBoundingBox().Contains(corners[5]) == ContainmentType.Contains)
                {
                    collision15 = true;
                    if (m is LavaBlock)
                    {
                        collisionWithLava = true;
                        lavaBlock = (LavaBlock)m;
                    }
                    
                    if (m is GrassBlock)
                        collisionWithGrass = true;
                    if (m is WaterBlock)
                        collisionWithWater = true;
                    if (m is StoneBlock)
                        collisionWithStone = true;
                    break;
                }
                else
                {
                    collision15 = false;
                    collisionWithLava = false;
                    collisionWithGrass = false;
                    collisionWithWater = false;
                    collisionWithStone = false;
                }
            }
            foreach (TerrainBlock m in groundBlocks)
            {
                if (m.getBoundingBox().Contains(corners[2]) == ContainmentType.Contains
                    && m.getBoundingBox().Contains(corners[6]) == ContainmentType.Contains && !(m is EnemyObstacle))
                {
                    collision26 = true;
                    break;
                }
                else
                    collision26 = false;
            }
            foreach (TerrainBlock m in groundBlocks)
            {
                if (m.getBoundingBox().Contains(corners[0]) == ContainmentType.Contains
                    && m.getBoundingBox().Contains(corners[4]) == ContainmentType.Contains && !(m is EnemyObstacle))
                {
                    collision04 = true;
                    break;
                }
                else
                    collision04 = false;
            }
            foreach (TerrainBlock m in groundBlocks)
            {
                if (m.getBoundingBox().Contains(corners[3]) == ContainmentType.Contains
                   && m.getBoundingBox().Contains(corners[7]) == ContainmentType.Contains && !(m is EnemyObstacle))
                {
                    collision37 = true;
                    break;
                }
                else
                    collision37 = false;
            }
            foreach (TerrainBlock block in groundBlocks)
            {
                if (!reachedEnd && !isDead && !block.isDealtWith() &&
                    block is EnemyObstacle && block.getBoundingBox().Contains(getPosition()) == ContainmentType.Contains)
                {
                    block.interact(this);

                }
                if ((!reachedEnd && !isDead && !block.isDealtWith() &&
                    block is SnareObstacle && block.getBoundingBox().Contains(getBoundingBox().GetCorners()[0]) == ContainmentType.Contains
                    && block.getBoundingBox().Contains(getBoundingBox().GetCorners()[1]) == ContainmentType.Contains))
                {
                    block.interact(this);
                }
            }
        }

        private void HandleCollision()
        {
            if (collision37)
            {
                nominalSpeed.X = -nominalSpeed.X;
                model.orientRotateY(MathHelper.Pi);
                facingRight = !facingRight;
            }
            if (collision26)
            {
                nominalSpeed.X = -nominalSpeed.X;
                model.orientRotateY(MathHelper.Pi);
                facingRight = !facingRight;
            }
            if (collision04)
            {
                if (!isJumping)
                    nominalSpeed.Y = 0;
                else
                    isJumping = false;
            }


            if (!collision04 && !collision15 && !collision26 && !collision37)
            {
                nominalSpeedPositive.X = 50;
                nominalSpeedPositive.Y = 50;
                nominalSpeedNegative.X = -50;
                nominalSpeedNegative.Y = -50;
                if(facingRight)
                    nominalSpeed.X = 20.0f;
                else
                    nominalSpeed.X = -20.0f;
                isFalling = true;
            }

        }

        private void HandleAdventurerState()
        {
            if (!isDead)
            {
                if (collisionWithLava)
                {
                    if (lavaAbility)
                    {
                        level.turnIntoStone(lavaBlock);
                        lavaAbilityUsed = true; 
                    }
                    else
                    {
                        impactEffectLavaInstance.Play();
                        Kill();
                    }
                }
                if (collisionWithWater)
                    swimmingEffectWaterInstance.Play();
                else
                    swimmingEffectWaterInstance.Stop();
                if (collisionWithGrass)
                {
                    if (lavaAbilityUsed)
                    {
                        lavaAbility = false;
                        lavaAbilityUsed = false;
                    }
                    footstepEffectGrassInstance.Play();
                }
                else
                    footstepEffectGrassInstance.Stop();

                if (collisionWithStone)
                {
                    if (lavaAbilityUsed)
                    {
                        lavaAbility = false;
                        lavaAbilityUsed = false;
                    }
                    footstepEffectStoneInstance.Play();
                }
                else
                    footstepEffectStoneInstance.Stop();
            }
        }


        private void Fall()
        {
            if (isFalling)
                acceleration.Y = GRAVITY;
            else
                acceleration.Y = 0;
            if(facingRight)
                nominalSpeed.X += acceleration.X;
            else
                nominalSpeed.X -= acceleration.X;
            nominalSpeed.Y -= acceleration.Y;
        }


        protected Vector2 FindLavaCoor(List<TerrainBlock> groundBlocks)
        {
            foreach (TerrainBlock m in groundBlocks)
            {
                if (m is LavaBlock)
                {
                    return new Vector2(m.getPosition().X, m.getPosition().Y);
                }
            }
            return Vector2.Zero;
        }

        private void ComputeDistances(Vector2 lavaPoint, Vector2 campfirePoint)
        {
            distanceToLava = (float)Math.Sqrt(
                Math.Pow(lavaPoint.X - position.X, 2.0)
                + Math.Pow(lavaPoint.Y - position.Y, 2.0));

            distanceToCampfire = (float)Math.Sqrt(
                Math.Pow(campfirePoint.X - position.X, 2.0)
                + Math.Pow(campfirePoint.Y - position.Y, 2.0));
        }

        private void HandleKeyboardInput()
        {
            KeyboardState keyboardState = Keyboard.GetState();

            if (keyboardState.IsKeyDown(Keys.J))
                move(nominalSpeedNegative.X, 0.0f, 0.0f);
            if (keyboardState.IsKeyDown(Keys.L))
                move(nominalSpeedPositive.X, 0.0f, 0.0f);
            if (keyboardState.IsKeyDown(Keys.I))
                move(0.0f, nominalSpeedPositive.Y, 0.0f);
            if (keyboardState.IsKeyDown(Keys.K))
                move(0.0f, nominalSpeedNegative.Y, 0.0f);
            if (keyboardState.IsKeyDown(Keys.Y))
                move(0.0f, 0.0f, nominalSpeedNegative.Z);
            if (keyboardState.IsKeyDown(Keys.H))
                move(0.0f, 0.0f, nominalSpeedPositive.Z);
        }
        public void Kill()
        {
            isDead = true;
            level.numberOfAdventurers--;
            level.score -= 20;
        }

    }
}
