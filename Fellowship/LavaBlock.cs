﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Fellowship
{
    public class LavaBlock : TerrainBlock 
    {

        public LavaBlock(Level l, int w, int h, int d): base ( l, w, h, d )
        {
            textureName = "Textures/lava";
        }


    }
}
