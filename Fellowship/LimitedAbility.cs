﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Fellowship
{
    abstract class LimitedAbility: Ability
    {
        public int NUMBER_OF_USES = 3;

        public abstract void Launch(Adventurer adv);

        public Texture2D icon
        {
            get;
            set;
        }

        public bool available()
        {
            if (NUMBER_OF_USES > 0)
                return true;
            return false;
        }

        public void Update(GameTime gameTime)
        {

        }
    }
}
