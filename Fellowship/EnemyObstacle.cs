﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Fellowship
{
    class EnemyObstacle: TerrainBlock
    {
        String textureName2; 
        public EnemyObstacle(Level l, int w, int h, int d)
            : base(l, w, h, d) {
                textureName = "Textures/Snail";
                textureName2 = "Textures/Transparent"; 
            }

        public override void interact(Adventurer a)
        {
            if(!(a is WarriorAdventurer))
            {
                a.Kill();
            }
            else
            {
                dealtWith = true;
            }
        }

        public override void LoadContent()
        {
            frontFace = modelManager.game.Content.Load<Texture2D>(@textureName);
            backFace = modelManager.game.Content.Load<Texture2D>(@textureName2);
            topFace = modelManager.game.Content.Load<Texture2D>(@textureName2);
            bottomFace = modelManager.game.Content.Load<Texture2D>(@textureName2);
            rightFace = modelManager.game.Content.Load<Texture2D>(@textureName2);
            leftFace = modelManager.game.Content.Load<Texture2D>(@textureName2);
            SetBuffers();
        }
    }
}
