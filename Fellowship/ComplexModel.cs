﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Fellowship
{
    class ComplexModel
    {
        protected VertexBuffer vertexBuffer;
        protected IndexBuffer indexBuffer;
        short[] indices;

        protected Game1 game;

        protected List<PrimitiveModel> models;


        public ComplexModel(Game1 g)
        {
            game = g;
            models = new List<PrimitiveModel>();

        }

        public void addModel(PrimitiveModel aModel)
        {
            if(aModel != null)
                models.Add(aModel);
        }
        public void removeModel(PrimitiveModel aModel)
        {
            if (aModel != null)
                models.Remove(aModel);
        }

        //return the drawing world orientation for the model
        public virtual Matrix getWorld()
        {

            return models[0].getWorld();
        }

        //translate the model from it current location
        public void translate(Matrix translationMatrix)
        {
            foreach (PrimitiveModel m in models)
            {
                m.translate(translationMatrix);
            }

        }
        //rotate about the X axis
        public void rotateX(Matrix rotateXmatrix)
        {
            foreach (PrimitiveModel m in models)
            {
                m.rotateX(rotateXmatrix);
            }

        }
        //rotate about the Y axis
        public void rotateY(Matrix rotateYmatrix)
        {
            foreach (PrimitiveModel m in models)
            {
                m.rotateY(rotateYmatrix);
            }
        }
        //scale
        public void scale(Matrix scaleMatrix)
        {
            foreach (PrimitiveModel m in models)
            {
                m.scale(scaleMatrix);
            }
        }
        //orbit about the origin
        public void orbit(Matrix orbitMatrix)
        {
            foreach (PrimitiveModel m in models)
            {
                m.orbit(orbitMatrix);
            }
        }


        public virtual void LoadContent()
        {
        }


        public virtual void Update(GameTime gameTime)
        {
            foreach (PrimitiveModel m in models)
            {
                m.Update(gameTime);
            }
        }

        public virtual void Draw(BasicEffect effect)
        {
            foreach (PrimitiveModel m in models)
            {
                m.Draw(effect);
            }

        }
    }
}
