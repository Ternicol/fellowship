﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Fellowship
{
    class Camera3D: Camera
    {
        private float slowdownMultiplier;
        private float forwardBackwardSpeed; //speed of camera movement
        private Vector3 cameraAngle;

        public Camera3D(Game1 game, Vector3 pos, Vector3 target, Vector3 up)
            : base(game, pos, target, up)
        {
            sideWaysSpeed = 100.0f; //speed of camera sideways movement
            upDownSpeed = 100.0f; //speed of camera up and down movement
            forwardBackwardSpeed = 100.0f;
            slowdownMultiplier = 1.0f;

            cameraTiltSpeed = 4.0f; //speed of camera tilt (Pitch) rotation
            cameraPanSpeed = 4.0f; //speed of camera pan (Yaw) rotation
            cameraRollSpeed = 1.0f; //speed of camera Roll rotation
        }

        public Camera3D(Game1 game, Vector3 pos, Vector3 target, Vector3 up, float speed, float turnSpeed)
            : base(game, pos, target, up)
        {

        }

        public override void Initialize()
        {
            // TODO: Add your initialization code here
            CreateLookAt();

            projection = Matrix.CreatePerspectiveFieldOfView(
                 MathHelper.PiOver4,  //field of view
                 (float)Game.Window.ClientBounds.Width / (float)Game.Window.ClientBounds.Height,  //aspect ration
                 1,  //near field distance
                 60000  //far field distance
                 );

            base.Initialize();
        }
        
        public override void Update(GameTime gameTime)
        {
            KeyboardState keyboardState = Keyboard.GetState();
            MouseState mouseState = Mouse.GetState();

            int center = viewport.Width / 2;

            int centerX = center;
            int centerY = center;

            Vector3 sideWaysDirection;

            Mouse.SetPosition(centerX, centerY);

            cameraAngle.X += MathHelper.ToRadians((mouseState.Y - centerY) * cameraTiltSpeed * 0.01f); // pitch
            cameraAngle.Y += MathHelper.ToRadians((mouseState.X - centerX) * cameraPanSpeed * 0.01f); // yaw

            cameraDirection = Vector3.Normalize(
                                new Vector3((float)Math.Sin(-cameraAngle.Y),
                                (float)Math.Sin(cameraAngle.X), (float)Math.Cos(-cameraAngle.Y))
                                );

            sideWaysDirection = Vector3.Normalize(new Vector3((float)Math.Cos(cameraAngle.Y),
                                0f,
                                (float)Math.Sin(cameraAngle.Y))
                                );

            if (keyboardState.IsKeyDown(Keys.LeftShift))
                slowdownMultiplier = 0.1f;
            else
                slowdownMultiplier = 1.0f;
            
            //Camera Forward and Backward Motion
            if (keyboardState.IsKeyDown(Keys.W))
            {
                cameraPosition -= cameraDirection * forwardBackwardSpeed * slowdownMultiplier; //move forward in the direction its facing
            }
            if (keyboardState.IsKeyDown(Keys.S))
                cameraPosition += cameraDirection * forwardBackwardSpeed * slowdownMultiplier; //move backward in the direction its facing

            //Camera Sideways Translation Motion
            if (keyboardState.IsKeyDown(Keys.A))
                cameraPosition -= sideWaysDirection * sideWaysSpeed * slowdownMultiplier; //move camera left
            if (keyboardState.IsKeyDown(Keys.D))
                cameraPosition += sideWaysDirection * sideWaysSpeed * slowdownMultiplier; //move camera right

            //Camera Up and Down Motion
            if (keyboardState.IsKeyDown(Keys.Up))
                cameraPosition += cameraUp * upDownSpeed * slowdownMultiplier; //move camera left
            if (keyboardState.IsKeyDown(Keys.Down))
                cameraPosition -= cameraUp * upDownSpeed * slowdownMultiplier; //move camera right


            CreateLookAt();  //rebuild camera view matrix

            base.Update(gameTime);
        }

        protected override void CreateLookAt()
        {
            //Create a LookAt view matrix using the camera direction
            //This will turn the camera direction and position into a target point
            view = Matrix.Identity;
            view *= Matrix.CreateTranslation(-cameraPosition);
            view *= Matrix.CreateRotationZ(cameraAngle.Z);
            view *= Matrix.CreateRotationY(cameraAngle.Y);
            view *= Matrix.CreateRotationX(cameraAngle.X);
        }
    }
}
