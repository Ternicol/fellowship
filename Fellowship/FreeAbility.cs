﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Fellowship
{
    class FreeAbility : LimitedAbility
    {

        public FreeAbility(Game game)
        {
            icon = game.Content.Load<Texture2D>(@"Icons/4");
        }
        public override void Launch(Adventurer adv)
        {
            if (available())
            {
                NUMBER_OF_USES--;
                adv.nominalSpeed = new Vector3(20.0f, 0.0f, 0.0f);
                adv.acceleration = new Vector2( 0,0.5f);
                 
            }

        }

    }
}