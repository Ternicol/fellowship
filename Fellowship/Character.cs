﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Fellowship
{
    class Character
    {
        CustomModel model;

        Vector3 position;
        public Vector3 size;
        public Vector3 speed { get; set; }

        public Character(CustomModel m)
        {
            model = m;

            position = Vector3.Zero;
            size = Vector3.One;
            speed = Vector3.Zero;
        }

        public Character(CustomModel m, Vector3 position, Vector3 size)
        {
            model = m;

            this.position = position;
            this.size = size;
            speed = Vector3.Zero;

            model.scale(Matrix.CreateScale(size.X, size.Y, size.Z));
            model.translate(Matrix.CreateTranslation(position.X, position.Y, position.Z));
            model.orientRotateY(MathHelper.PiOver2);
        }

        public bool intersects(int x, int y)
        {
            if(model.getBoundingBox().Contains(new Vector3(x, y, 0)) == ContainmentType.Contains)
                return true;
            return false;
        }

        public void move(float x, float y, float z)
        {
            model.translate(Matrix.CreateTranslation(x, y, z));
        }

        public BoundingBox getBoundingBox()
        {
            return model.getBoundingBox();
        }

        public Vector3 getPosition()
        {
            return model.getPosition();
        }

        public void Update()
        {
            model.Update();
        }

        public void Draw(Camera camera)
        {
            model.Draw(camera);
        }
    }
}
