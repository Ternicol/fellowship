﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Fellowship
{
    class AbilityUI: Microsoft.Xna.Framework.DrawableGameComponent
    {

        internal GraphicsDeviceManager graphics;

        Rectangle startingCoor;

        Adventurer adventurer;
        List<Ability> abilities;
        List<Rectangle> iconAreas = new List<Rectangle>();

        Level level;

        MouseState currentMouseState;
        MouseState previousMouseState;

        protected SoundEffect abilityUseEffect;
        public SoundEffectInstance abilityUseEffectInstance;

        public AbilityUI(Game1 game): base(game)
        {
            level = game.modelManager;

            graphics = game.graphics;

            startingCoor = new Rectangle(
                5,
                graphics.PreferredBackBufferHeight/2 - 200,
                80, 80);
        }

        public override void Initialize()
        {
            adventurer = level.activeAdventurer;
            abilities = adventurer.abilities;

            for (int i = 0; i < abilities.Count; i++)
            {
                iconAreas.Add(new Rectangle(
                    startingCoor.X, startingCoor.Y + 100 * i,
                    startingCoor.Width, startingCoor.Height));
            }

            base.Initialize();
        }


        protected override void LoadContent()
        {
            abilityUseEffect = level.game.Content.Load<SoundEffect>(@"Sounds/ability_use"); 
        }

        public override void Update(GameTime gameTime)
        {
            adventurer = level.activeAdventurer;
            abilities = adventurer.abilities;

            iconAreas.Clear();
            for (int i = 0; i < abilities.Count; i++)
            {
                iconAreas.Add(new Rectangle(
                    startingCoor.X, startingCoor.Y + 100 * i,
                    startingCoor.Width, startingCoor.Height));
            }

            foreach(Adventurer adv in level.adventurers)
            {
                foreach (Ability ab in adv.abilities)
                {
                    ab.Update(gameTime);
                }
            }

            HandleMouse();

            base.Update(gameTime);
        }

        private void HandleMouse()
        {
            currentMouseState = Mouse.GetState();
            
            for (int i = 0; i < iconAreas.Count; i++)
            {
                Rectangle rect = iconAreas.ElementAt(i);

                if (rect.Contains(new Point(previousMouseState.X, previousMouseState.Y))
                    && previousMouseState.LeftButton == ButtonState.Pressed)
                {
                    if (abilities.ElementAt(i).available())
                    {
                        abilityUseEffectInstance = abilityUseEffect.CreateInstance();
                        abilityUseEffectInstance.Volume = 0.3f;
                        abilityUseEffectInstance.Play();
                    }
                    if (currentMouseState.LeftButton == ButtonState.Released)
                    {
                        if(abilities.Count > 0)
                            abilities.ElementAt(i).Launch(adventurer);
  
                    }
                }
            }
            previousMouseState = Mouse.GetState();
        }



        public override void Draw(GameTime gameTime)
        {

            ((Game1)Game).spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, RasterizerState.CullNone);
            if (!adventurer.isDead)
            {
                for (int i = 0; i < abilities.Count; i++)
                {
                    Ability ab = abilities.ElementAt(i);

                    if (!ab.available())
                    {
                        ((Game1)Game).spriteBatch.Draw(ab.icon, iconAreas.ElementAt(i), Color.Blue);
                        if (ab is CooldownAbility)
                            DrawTimer(i, gameTime);
                    }
                    else
                        ((Game1)Game).spriteBatch.Draw(ab.icon, iconAreas.ElementAt(i), Color.White);
                }
            }
            ((Game1)Game).spriteBatch.End();
            base.Draw(gameTime);
        }

        public void DrawTimer(int i, GameTime gameTime)
        {
            CooldownAbility ab = ((CooldownAbility)abilities.ElementAt(i));
            String timerString = ab.cooldown.Seconds.ToString("00");
            ((Game1)Game).spriteBatch.DrawString(((Game1)Game).datafont, timerString,
                new Vector2(iconAreas.ElementAt(i).X+10, iconAreas.ElementAt(i).Y+5), Color.Red,
                0.0f, Vector2.Zero, new Vector2(2.0f, 2.0f), SpriteEffects.None, 1.0f);
        }

    }
}
