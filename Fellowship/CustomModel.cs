﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Fellowship
{
    public class CustomModel
    {
        protected VertexBuffer vertexBuffer;
        protected IndexBuffer indexBuffer;
        short[] indices;

        public Model model { get; protected set; }  //Model is XNA  class that represents 3D models in memory
        protected Matrix world = Matrix.Identity;

        //matrices used to locate model in the world
        protected Matrix worldSpinXMatrix; //rotation about X axis
        protected Matrix worldSpinYMatrix; //rotation about Y axis
        protected Matrix worldSpinZMatrix; //rotation about Z axis
        protected Matrix worldOrbitMatrix; //rotation aobut origin
        protected Matrix worldTranslationMatrix; //location of model in the world
        protected Matrix worldScaleMatrix; //world scaling of model

        internal Matrix[] m_transforms;


        //matrices used to orient model relative of other model parts
        //e.g. to position the wing of an airplane relative to the fuselage
        protected Matrix dimensionScaleMatrix;
        protected Matrix orientationMatrix;
        protected Matrix orientTranslationMatrix;

        public CustomModel(Model m)
        {
            model = m;
            m_transforms = new Matrix[model.Bones.Count];

            model.CopyBoneTransformsTo(m_transforms);

            worldTranslationMatrix = Matrix.Identity;
            worldSpinXMatrix = Matrix.Identity;
            worldSpinYMatrix = Matrix.Identity;
            worldSpinZMatrix = Matrix.Identity;
            worldOrbitMatrix = Matrix.Identity;
            worldScaleMatrix = Matrix.Identity;
            dimensionScaleMatrix = Matrix.Identity;
            orientationMatrix = Matrix.Identity;
            orientTranslationMatrix = Matrix.Identity;
        }


        public void orientRotateX(float rx)
        {
            orientationMatrix *= Matrix.CreateRotationX(rx);
        }

        public void orientRotateY(float ry)
        {
            orientationMatrix *= Matrix.CreateRotationY(ry);
        }
        public void orientRotateZ(float rz)
        {
            orientationMatrix *= Matrix.CreateRotationZ(rz);
        }

        public void orientTranslate(float tx, float ty, float tz)
        {
            orientTranslationMatrix *= Matrix.CreateTranslation(new Vector3(tx, ty, tz));

        }

        //return the drawing world orientation for the model
        public virtual Matrix getWorldTranslation()
        {
            return worldTranslationMatrix;
        }

        //return the drawing world orientation for the model
        public virtual Matrix getWorldScale()
        {
            return worldScaleMatrix;
        }

        //return the drawing world orientation for the model
        public virtual Matrix getWorldRotation()
        {

            return worldTranslationMatrix;
        }

        //translate the model from it current location
        public void translate(Matrix translationMatrix)
        {
            worldTranslationMatrix *= translationMatrix;
        }
        //rotate about the X axis
        public void rotateX(Matrix rotateXmatrix)
        {
            worldSpinXMatrix *= rotateXmatrix;
        }
        //rotate about the Y axis
        public void rotateY(Matrix rotateYmatrix)
        {
            worldSpinYMatrix *= rotateYmatrix;
        }
        //rotate about the Z axis
        public void rotateZ(Matrix rotateZmatrix)
        {
            worldSpinZMatrix *= rotateZmatrix;
        }

        //scale
        public void scale(Matrix scaleMatrix)
        {
            worldScaleMatrix *= scaleMatrix;
        }
        //orbit about the origin
        public void orbit(Matrix orbitMatrix)
        {
            worldOrbitMatrix *= orbitMatrix;
        }


        public virtual Matrix getWorld()
        {
            return world;
        }

        public Vector3 getPosition()
        {
            return Matrix.Invert(getWorldTranslation()).Translation;
        }

        public BoundingBox getBoundingBox()
        {

        // Create variables to hold min and max xyz values for the model. Initialise them to extremes
        Vector3 modelMax = new Vector3(float.MinValue, float.MinValue, float.MinValue);
        Vector3 modelMin = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);

        foreach (ModelMesh mesh in model.Meshes)
        {
            //Create variables to hold min and max xyz values for the mesh. Initialise them to extremes
            Vector3 meshMax = new Vector3(float.MinValue, float.MinValue, float.MinValue);
            Vector3 meshMin = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);

              // There may be multiple parts in a mesh (different materials etc.) so loop through each
              foreach (ModelMeshPart part in mesh.MeshParts)
              {
                 // The stride is how big, in bytes, one vertex is in the vertex buffer
                 // We have to use this as we do not know the make up of the vertex
                 int stride = part.VertexBuffer.VertexDeclaration.VertexStride;

                 byte[] vertexData = new byte[stride * part.NumVertices]; 
                 part.VertexBuffer.GetData(part.VertexOffset * stride, vertexData, 0, part.NumVertices, 1); // fixed 13/4/11

                 // Find minimum and maximum xyz values for this mesh part
                 // We know the position will always be the first 3 float values of the vertex data
                 Vector3 vertPosition=new Vector3();
                 for (int ndx = 0; ndx < vertexData.Length; ndx += stride)
                 { 
                     vertPosition.X= BitConverter.ToSingle(vertexData, ndx);
                     vertPosition.Y = BitConverter.ToSingle(vertexData, ndx + sizeof(float));
                     vertPosition.Z= BitConverter.ToSingle(vertexData, ndx + sizeof(float)*2);

                     // update our running values from this vertex

                     meshMin = Vector3.Min(meshMin, vertPosition);
                     meshMax = Vector3.Max(meshMax, vertPosition);
                 }
               }

               // transform by mesh bone transforms
               meshMin = Vector3.Transform(meshMin, m_transforms[mesh.ParentBone.Index]);
               meshMax = Vector3.Transform(meshMax, m_transforms[mesh.ParentBone.Index]);

               // Expand model extents by the ones from this mesh
               modelMin = Vector3.Min(modelMin, meshMin);
               modelMax = Vector3.Max(modelMax, meshMax);
            }

            modelMin = Vector3.Transform(modelMin, getWorldScale());
            modelMax = Vector3.Transform(modelMax, getWorldScale());

            modelMin += Matrix.Invert(getWorldTranslation()).Translation;
            modelMax += Matrix.Invert(getWorldTranslation()).Translation;

            // Create and return the model bounding box
            return new BoundingBox(modelMin, modelMax);
        }



        public virtual void Update()
        {
            //to be overidden by sub-classes
          //  world = dimensionScaleMatrix;
            world = worldScaleMatrix;
            world *= orientationMatrix;
            world *= orientTranslationMatrix;

            world *= worldSpinXMatrix * worldSpinYMatrix * worldSpinZMatrix * worldTranslationMatrix * worldOrbitMatrix;
        }

        public void Draw(Camera camera)
        {
            Matrix[] transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);

            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect be in mesh.Effects)
                {
                    be.EnableDefaultLighting();
                    be.Projection = camera.projection;
                    be.View = camera.view;
                    be.World = getWorld() * mesh.ParentBone.Transform;
                }
                mesh.Draw();
            }
        }
    }
}
