﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Fellowship
{
    public class Level1: Level
    {

        public Level1(Game game)
            : base(game)
        {
            // TODO: Construct any child components here
            this.game = game;
            timer = new TimeSpan(0, 3, 0);
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            // TODO: Add your initialization code here
            endArea = new ActivatorArea(new Rectangle(19000, -1500, 2000, 2000), -10000);

            layers.Add(new Background(this, 60000, 40000, -25000));
            layers.Add(new Background(this, 40000, 20000, -24000));
            layers.Add(new Background(this, 40000, 20000, -23000));
            layers.Add(new Background(this, 40000, 15000, -22000));
            layers.Add(new Background(this, 40000, 15000, -21000));

            terrain = new Terrain(@"Content\LevelMaps\levelone.txt", 9, 40, this); //Initialize a level that is 10x10 blocks large. 
            terrain.initializeTerrain(-18000, 1000, -10000, 1000);       //Sets position of the level and block size. 

            adventurers.Add(
                new WarriorAdventurer(this,
                new CustomModel(Game.Content.Load<Model>(@"Models/spaceship")),
                new Vector3(-18000, 2800, -10000), new Vector3(30, 30, 30)));
            
            adventurers.Add(
                new ArcherAdventurer(this,
                new CustomModel(Game.Content.Load<Model>(@"Models/spaceship")),
                new Vector3(-18000, 2800, -10000), new Vector3(30, 30, 30)));

            activeAdventurer = adventurers.First();
            activeAdventurer.isSelected = true;

            numberOfAdventurers = adventurers.Count;
            score = 0;

            for (int i = 0; i < adventurers.Count; i++)
            {
                spawnTimes.Add(5*i + 3);
            }

            base.Initialize();
        }

        protected override void LoadContent()
        {
            string[] layerTexture = { 
                "Textures/layer1",
                "Textures/layer2",
                "Textures/layer3",
                "Textures/layer4",
                "Textures/layer5",
                "Textures/layer6",
                "Textures/layer7",
                "Textures/layer8",
                "Textures/layer9",
            };

            for (int i = 0; i < layers.Count; i++)
            {
                string[] t = { layerTexture[i] };
                layers[i].LoadContent(t);
            }

            endArea.LoadTexture(this, new string[] { @"Textures/campfire" });
            
            terrain.initializeTextures();
            for (int i = 0; i < terrain.getSizeX(); i++)
            {
                for (int p = 0; p < terrain.getSizeY(); p++)
                {
                    //Adds the blocks to the models list so that they are drawn. 
                    if (terrain.getBlockGuide(i, p) != '0') { groundBlocks.Add(terrain.getBlock(i, p)); }
                }
            }

            effect = new BasicEffect(GraphicsDevice);

            ambiant = ((Game1)Game).Content.Load<SoundEffect>(@"Sounds/Low Wind & Tone Atmosphere");
            ambiantInstance = ambiant.CreateInstance();
            ambiantInstance.IsLooped = true;
            ambiantInstance.Volume = 1.0f;
            ambiantInstance.Play();

            music = ((Game1)Game).Content.Load<Song>("Music/test_music");
       //     MediaPlayer.Volume = 0.5f;
       //     MediaPlayer.IsRepeating = true;
       //     MediaPlayer.Play(music);

            base.LoadContent();
        }

    }
}
