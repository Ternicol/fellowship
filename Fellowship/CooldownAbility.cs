﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Fellowship
{
    abstract class CooldownAbility: Ability
    {
        public TimeSpan cooldown = new TimeSpan(0, 0, 30);
        protected bool activated = false;

        public Texture2D icon
        {
            get;
            set;
        }

        public abstract void Launch(Adventurer adv);

        public bool available()
        {
            if(!activated)
                return true;
            return false;
        }

        public void Update(GameTime gameTime)
        {
            if (activated)
            {
                if (cooldown.Seconds >= 0)
                    cooldown -= gameTime.ElapsedGameTime;
                else
                {
                    cooldown = new TimeSpan(0, 0, 30);
                    activated = false;
                }
            }
        }
    }
}
