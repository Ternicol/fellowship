﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Fellowship
{
    public class PrimitiveModel
    {
        protected VertexBuffer vertexBuffer;
        protected IndexBuffer indexBuffer;
        protected short[] indices;

        protected Level modelManager;
        protected Matrix world = Matrix.Identity;

        //matrices used to locate model in the world
        protected Matrix worldSpinXMatrix; //rotation about X axis
        protected Matrix worldSpinYMatrix; //rotation about Y axis
        protected Matrix worldSpinZMatrix; //rotation about Z axis
        protected Matrix worldOrbitMatrix; //rotation aobut origin
        protected Matrix worldTranslationMatrix; //location of model in the world
        protected Matrix worldScaleMatrix; //world scaling of model


        //matrices used to orient model relative of other model parts
        //e.g. to position the wing of an airplane relative to the fuselage
        protected Matrix dimensionScaleMatrix;
        protected Matrix orientationMatrix;
        protected Matrix orientTranslationMatrix;


        public PrimitiveModel(Level modelManager)
        {
            this.modelManager = modelManager;

            dimensionScaleMatrix = Matrix.Identity;
            orientationMatrix = Matrix.Identity;
            orientTranslationMatrix = Matrix.Identity;
            worldTranslationMatrix = Matrix.Identity;
            worldSpinXMatrix = Matrix.Identity;
            worldSpinYMatrix = Matrix.Identity;
            worldSpinZMatrix = Matrix.Identity;
            worldOrbitMatrix = Matrix.Identity;
            worldScaleMatrix = Matrix.Identity;
        }

        public void orientRotateX(float rx)
        {
            orientationMatrix *= Matrix.CreateRotationX(rx);
        }

        public void orientRotateY(float ry)
        {
            orientationMatrix *= Matrix.CreateRotationY(ry);
        }
        public void orientRotateZ(float rz)
        {
            orientationMatrix *= Matrix.CreateRotationZ(rz);
        }

        public void orientTranslate(float tx, float ty, float tz)
        {
            orientTranslationMatrix *= Matrix.CreateTranslation(new Vector3(tx, ty, tz));

        }

        //return the drawing world orientation for the model
        public virtual Matrix getWorld()
        {
            return world;
        }

        //return the drawing world orientation for the model
        public virtual Matrix getWorldTranslation()
        {
            return worldTranslationMatrix;
        }

        //return the drawing world orientation for the model
        public virtual Matrix getWorldRotation()
        {

            return worldTranslationMatrix;
        }

        //translate the model from it current location
        public void translate(Matrix translationMatrix)
        {
            worldTranslationMatrix *= translationMatrix;
        }
        //rotate about the X axis
        public void rotateX(Matrix rotateXmatrix)
        {
            worldSpinXMatrix *= rotateXmatrix;
        }
        //rotate about the Y axis
        public void rotateY(Matrix rotateYmatrix)
        {
            worldSpinYMatrix *= rotateYmatrix;
        }
        //rotate about the Z axis
        public void rotateZ(Matrix rotateZmatrix)
        {
            worldSpinZMatrix *= rotateZmatrix;
        }

        //scale
        public void scale(Matrix scaleMatrix)
        {
            worldScaleMatrix *= scaleMatrix;
        }
        //orbit about the origin
        public void orbit(Matrix orbitMatrix)
        {
            worldOrbitMatrix *= orbitMatrix;
        }

        public virtual void LoadContent()
        {
            //subclasses should override this
        }
        public virtual void LoadContent(string[] textures)
        {
            //subclasses should override this
        }

        public virtual void Update(GameTime gameTime)
        {
            //subclasses should override this
        }

        public virtual void Draw(BasicEffect effect)
        {
            //subclasses should override this
        }


    }
}
