﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Fellowship
{
    class TurnAbility: CooldownAbility
    {

        public TurnAbility(Game game)
        {
            icon = game.Content.Load<Texture2D>(@"Icons/1");
        }

        public override void Launch(Adventurer adv)
        {
            if (!activated)
            {
                adv.nominalSpeed.X = -adv.nominalSpeed.X;
                adv.model.orientRotateY(MathHelper.Pi);
                adv.facingRight = !adv.facingRight;
                activated = true;
            }
            
        }
    }
}
