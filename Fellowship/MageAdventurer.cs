﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Fellowship
{
    class MageAdventurer : Adventurer
    {

        public MageAdventurer(Level level, CustomModel m, Vector3 position, Vector3 size)
            : base(level, m, position, size)
        {
            icon = level.game.Content.Load<Texture2D>(@"Icons/portrait1");
            abilities.Add(new EndAbility(level.game));
            abilities.Add(new LavaAbility(level.game)); 
            abilities.Add(new FreeAbility(level.game));
            abilities.Add(new TurnAbility(level.game));
            abilities.Add(new JumpAbility(level.game));
        }
        
    }
}
