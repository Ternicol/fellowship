﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Fellowship
{
    public class Background: PrimitiveModel
    {

        Texture2D backdropTexture;

        float width;
        float height;

        VertexPositionTexture[] vertexTextures; //vertices of triangles forming a quad

        public Vector3? parallax { get; set; }
        float? parallaxSpeed { get; set; }

        public Background(Level modelManager, float width, float height, float z)
            :base(modelManager)
        {
            vertexTextures = new VertexPositionTexture[6];

            this.width = width;
            this.height = height;

            dimensionScaleMatrix = Matrix.CreateScale(width/2, height/2, 0);
            worldTranslationMatrix = Matrix.CreateTranslation(0, 0, z);

            vertexTextures[0] = new VertexPositionTexture(new Vector3(-1, 1, 0), new Vector2(0, 0));
            vertexTextures[1] = new VertexPositionTexture(new Vector3(1, 1, 0), new Vector2(1, 0));
            vertexTextures[2] = new VertexPositionTexture(new Vector3(-1, -1, 0), new Vector2(0, 1));
            vertexTextures[3] = new VertexPositionTexture(new Vector3(1, 1, 0), new Vector2(1, 0));
            vertexTextures[4] = new VertexPositionTexture(new Vector3(1, -1, 0), new Vector2(1, 1));
            vertexTextures[5] = new VertexPositionTexture(new Vector3(-1, -1, 0), new Vector2(0, 1));

            indices = new short[]
            {
                0, 1, 2, 3, 4, 5
            };

        }

        public Background(Level modelManager, float width, float height, float x, float y, float z)
            :base(modelManager)
        {
            vertexTextures = new VertexPositionTexture[6];

            dimensionScaleMatrix = Matrix.CreateScale(width / 2, height / 2, 0);
            worldTranslationMatrix = Matrix.CreateTranslation(x, y, z);

            vertexTextures[0] = new VertexPositionTexture(new Vector3(-1, 1, 0), new Vector2(0, 0));
            vertexTextures[1] = new VertexPositionTexture(new Vector3(1, 1, 0), new Vector2(1, 0));
            vertexTextures[2] = new VertexPositionTexture(new Vector3(-1, -1, 0), new Vector2(0, 1));
            vertexTextures[3] = new VertexPositionTexture(new Vector3(1, 1, 0), new Vector2(1, 0));
            vertexTextures[4] = new VertexPositionTexture(new Vector3(1, -1, 0), new Vector2(1, 1));
            vertexTextures[5] = new VertexPositionTexture(new Vector3(-1, -1, 0), new Vector2(0, 1));

            indices = new short[]
            {
                0, 1, 2, 3, 4, 5
            };

        }


        public Background(Level level, float width, float height, float z, Vector3 parallax, float parallaxSpeed)
            : base(level)
        {
            vertexTextures = new VertexPositionTexture[6];

            this.width = width;
            this.height = height;

            dimensionScaleMatrix = Matrix.CreateScale(width / 2, height / 2, 0);
            worldTranslationMatrix = Matrix.CreateTranslation(0, 0, z);

            vertexTextures[0] = new VertexPositionTexture(new Vector3(-1, 1, 0), new Vector2(0, 0));
            vertexTextures[1] = new VertexPositionTexture(new Vector3(1, 1, 0), new Vector2(1, 0));
            vertexTextures[2] = new VertexPositionTexture(new Vector3(-1, -1, 0), new Vector2(0, 1));
            vertexTextures[3] = new VertexPositionTexture(new Vector3(1, 1, 0), new Vector2(1, 0));
            vertexTextures[4] = new VertexPositionTexture(new Vector3(1, -1, 0), new Vector2(1, 1));
            vertexTextures[5] = new VertexPositionTexture(new Vector3(-1, -1, 0), new Vector2(0, 1));

            indices = new short[]
            {
                0, 1, 2, 3, 4, 5
            };

            this.parallax = parallax;
            this.parallaxSpeed = parallaxSpeed;

        }

        private void SetBuffers()
        {
            vertexBuffer = new VertexBuffer(
                modelManager.GraphicsDevice, 
                typeof(VertexPositionTexture),
                vertexTextures.Length, 
                BufferUsage.None
                );

            vertexBuffer.SetData<VertexPositionTexture>(vertexTextures);

            indexBuffer = new IndexBuffer(
                modelManager.GraphicsDevice,
                IndexElementSize.SixteenBits,
                sizeof(short) * indices.Length, 
                BufferUsage.None
                );

            indexBuffer.SetData<short>(indices);
        }

        public override void LoadContent(string[] textures)
        {
            
            backdropTexture = modelManager.game.Content.Load<Texture2D>(@textures[0]);
            SetBuffers();
        }

        public override void Update(GameTime gameTime)
        {
            world = dimensionScaleMatrix;
            world *= orientationMatrix;
            world *= orientTranslationMatrix;

            world *= worldSpinXMatrix * worldSpinYMatrix * worldSpinZMatrix * worldTranslationMatrix * worldOrbitMatrix;

            base.Update(gameTime);
        }


        public override void Draw(BasicEffect effect)
        {
            
            effect.World = world; //set position to draw model
            effect.Texture = backdropTexture;

            effect.VertexColorEnabled = false;  //because we are about to render textures not color
            effect.TextureEnabled = true;  //because we are about to render textures not colors

            modelManager.GraphicsDevice.Indices = indexBuffer;
            modelManager.GraphicsDevice.SetVertexBuffer(vertexBuffer);


            effect.CurrentTechnique.Passes[0].Apply();
            modelManager.GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, 6, 0, 2);


        }

    }
}
