﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace Fellowship
{
    public class ActivatorArea
    {

        internal Rectangle area;
        internal Background texture;

        internal int z;

        public ActivatorArea(Rectangle r, int z)
        {
            area = r;
            this.z = z;
        }

        public ActivatorArea(Background b, Rectangle r, int z)
        {
            texture = b;
            area = r;
            this.z = z;
        }

        public void LoadTexture(Level level, string[] textures)
        {
            texture = new Background(level, area.Width, area.Height, area.X, area.Y, z);
            texture.LoadContent(textures);
        }

        public Vector3 getPosition()
        {
            return Matrix.Invert(texture.getWorldTranslation()).Translation;
        }

        public Vector3 getRotation()
        {
            return Matrix.Invert(texture.getWorldRotation()).Translation;
        }


        public BoundingBox getBoundingBox()
        {
            Vector3 pos = getPosition();
            Vector3 angle = getRotation();

            Vector3[] points = 
            {
                new Vector3(pos.X, pos.Y, pos.Z - area.Width),
                new Vector3(pos.X + area.Width, pos.Y, pos.Z - area.Width),
                new Vector3(pos.X, pos.Y + area.Height, pos.Z - area.Width),
                new Vector3(pos.X + area.Width, pos.Y + area.Height, pos.Z - area.Width),

                new Vector3(pos.X, pos.Y, pos.Z + area.Width),
                new Vector3(pos.X + area.Width, pos.Y, pos.Z + area.Width),
                new Vector3(pos.X, pos.Y + area.Height, pos.Z + area.Width),
                new Vector3(pos.X + area.Width, pos.Y + area.Height, pos.Z + area.Width)
            };

            return BoundingBox.CreateFromPoints(points);
        }

    }
}
