﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Fellowship
{
    class PrismModel: PrimitiveModel
    {

        int numberOfFaces;
        float nominal_radius;
        float nominal_height;

        bool wrap;

        Texture2D frontFaceTexture;
        Texture2D backFaceTexture;
        Texture2D[] bodyTextures;

        VertexPositionTexture[] vertexTextures; //vertices of triangles with texture mapping that form the faces
        VertexPositionTexture[] vertexTopEndTextures; //vertices of triangles with texture mapping that form an end
        VertexPositionTexture[] vertexBottomEndTextures; //vertices of triangles with texture mapping that form an end

        public PrismModel(Level1 modelManager, int nFaces)
            : base(modelManager)
        {
            bodyTextures = new Texture2D[nFaces];
            InitializeModel(nFaces);
            wrap = false;
        }

        public PrismModel(Level1 modelManager, int nFaces, float radius, float height)
            : base(modelManager)
        {
            bodyTextures = new Texture2D[nFaces];
            InitializeModel(nFaces);
            dimensionScaleMatrix = Matrix.CreateScale(radius / nominal_radius, height / nominal_height, radius / nominal_radius);
            wrap = false;
        }

        public PrismModel(Level1 modelManager, int nFaces, float radius, float height, bool wrap)
            : base(modelManager)
        {
            bodyTextures = new Texture2D[nFaces];
            InitializeModel(nFaces);
            dimensionScaleMatrix = Matrix.CreateScale(radius / nominal_radius, height / nominal_height, radius / nominal_radius);
            this.wrap = wrap;
        }


        private void InitializeModel(int numFaces)
        {
            dimensionScaleMatrix = Matrix.Identity;
            orientationMatrix = Matrix.Identity;
            orientTranslationMatrix = Matrix.Identity;

            numberOfFaces = numFaces;

            nominal_radius = 2.0f;
            nominal_height = 2.0f;

            vertexTextures = new VertexPositionTexture[numberOfFaces * 2 * 3];  //not including top and bottom ends
            vertexTopEndTextures = new VertexPositionTexture[numberOfFaces * 3];  //for top end triangles
            vertexBottomEndTextures = new VertexPositionTexture[numberOfFaces * 3];  //bottom end triangles

            //for temporary use
            List<VertexPositionTexture> vertices = new List<VertexPositionTexture>();
            List<VertexPositionTexture> topEndVertices = new List<VertexPositionTexture>();
            List<VertexPositionTexture> bottomEndVertices = new List<VertexPositionTexture>();

            float angle = MathHelper.Pi * 2 / numberOfFaces; //angle of each face

            Vector3[] topVertices = new Vector3[numberOfFaces + 1];
            Vector3[] bottomVertices = new Vector3[numberOfFaces + 1];
            float[] textureX = new float[numberOfFaces + 1];

            Vector3 v0top = new Vector3(1, 1, 0);
            Vector3 v0bottom = new Vector3(1, -1, 0);

            Vector3 topCenter = new Vector3(0, 1, 0);
            Vector3 bottomCenter = new Vector3(0, -1, 0);

            topVertices[0] = v0top;
            topVertices[numberOfFaces] = v0top;
            bottomVertices[0] = v0bottom;
            bottomVertices[numberOfFaces] = v0bottom;
            textureX[0] = 0.0f;
            textureX[numberOfFaces] = 1.0f;


            //define the vertices around the top and bottom of prism
            for (int i = 1; i < numberOfFaces; i++)
            {
                Matrix rotationMatrix = Matrix.CreateRotationY(angle * i);
                topVertices[i] = Vector3.Transform(v0top, rotationMatrix);
                bottomVertices[i] = Vector3.Transform(v0bottom, rotationMatrix);
                textureX[i] = (float)i / numberOfFaces;
            }

            if (wrap)
            {
                //define the triangle pairs that make up each face
                for (int i = 0; i < numberOfFaces; i++)
                {
                    //face vertices -in clockwise render order
                    vertices.Add(new VertexPositionTexture(topVertices[i], new Vector2(textureX[i], 0)));
                    vertices.Add(new VertexPositionTexture(topVertices[i + 1], new Vector2(textureX[i + 1], 0)));
                    vertices.Add(new VertexPositionTexture(bottomVertices[i], new Vector2(textureX[i], 1)));

                    vertices.Add(new VertexPositionTexture(bottomVertices[i], new Vector2(textureX[i], 1)));
                    vertices.Add(new VertexPositionTexture(topVertices[i + 1], new Vector2(textureX[i + 1], 0)));
                    vertices.Add(new VertexPositionTexture(bottomVertices[i + 1], new Vector2(textureX[i + 1], 1)));

                    //define triangle that makes up portion of prism top end scribed by the current face
                    topEndVertices.Add(new VertexPositionTexture(
                        topVertices[i], 
                        new Vector2(0.5f + topVertices[i].X / 2.0f, 0.5f + topVertices[i].Z / 2.0f)));
                    topEndVertices.Add(new VertexPositionTexture(
                        topCenter, 
                        new Vector2(0.5f, 0.5f)));
                    topEndVertices.Add(new VertexPositionTexture(
                        topVertices[i + 1], 
                        new Vector2(0.5f + topVertices[i + 1].X / 2.0f, 0.5f + topVertices[i + 1].Z / 2.0f)));

                    //define triangle that makes up portion of prism bottom end scribed by the current face
                    bottomEndVertices.Add(new VertexPositionTexture(
                        bottomCenter, 
                        new Vector2(0.5f, 0.5f)));
                    bottomEndVertices.Add(new VertexPositionTexture(
                        bottomVertices[i], 
                        new Vector2(0.5f + bottomVertices[i].X / 2.0f, 0.5f + bottomVertices[i].Z / 2.0f)));
                    bottomEndVertices.Add(new VertexPositionTexture(
                        bottomVertices[i + 1], 
                        new Vector2(0.5f + bottomVertices[i + 1].X / 2.0f, 0.5f + bottomVertices[i + 1].Z / 2.0f)));
                }
            }
            else
            {
                //define the triangle pairs that make up each face
                for (int i = 0; i < numberOfFaces; i++)
                {
                    //face vertices -in clockwise render order
                    vertices.Add(new VertexPositionTexture(topVertices[i], new Vector2(0, 0)));
                    vertices.Add(new VertexPositionTexture(topVertices[i + 1], new Vector2(1, 0)));
                    vertices.Add(new VertexPositionTexture(bottomVertices[i], new Vector2(0, 1)));

                    vertices.Add(new VertexPositionTexture(bottomVertices[i], new Vector2(0, 1)));
                    vertices.Add(new VertexPositionTexture(topVertices[i + 1], new Vector2(1, 0)));
                    vertices.Add(new VertexPositionTexture(bottomVertices[i + 1], new Vector2(1, 1)));

                    //define triangle that makes up portion of prism top end scribed by the current face
                    topEndVertices.Add(new VertexPositionTexture(topVertices[i], new Vector2(0.5f + topVertices[i].X / 2.0f, 0.5f + topVertices[i].Z / 2.0f)));
                    topEndVertices.Add(new VertexPositionTexture(topCenter, new Vector2(0.5f, 0.5f)));
                    topEndVertices.Add(new VertexPositionTexture(topVertices[i + 1], new Vector2(0.5f + topVertices[i + 1].X / 2.0f, 0.5f + topVertices[i + 1].Z / 2.0f)));

                    //define triangle that makes up portion of prism bottom end scribed by the current face
                    bottomEndVertices.Add(new VertexPositionTexture(bottomCenter, new Vector2(0.5f, 0.5f)));
                    bottomEndVertices.Add(new VertexPositionTexture(bottomVertices[i], new Vector2(0.5f + bottomVertices[i].X / 2.0f, 0.5f + bottomVertices[i].Z / 2.0f)));
                    bottomEndVertices.Add(new VertexPositionTexture(bottomVertices[i + 1], new Vector2(0.5f + bottomVertices[i + 1].X / 2.0f, 0.5f + bottomVertices[i + 1].Z / 2.0f)));
                }
            }

            //transfer vertices to arrays
            for (int i = 0; i < vertices.Count(); i++)
                vertexTextures[i] = vertices[i];

            for (int i = 0; i < topEndVertices.Count(); i++)
                vertexTopEndTextures[i] = topEndVertices[i];

            for (int i = 0; i < bottomEndVertices.Count(); i++)
                vertexBottomEndTextures[i] = bottomEndVertices[i];
        }


        public override void LoadContent(string[] textures)
        {
            //textures is expected to have three textures: faces, top end, bottom end

            bodyTextures[0] = modelManager.game.Content.Load<Texture2D>(@textures[0]);
            frontFaceTexture = modelManager.game.Content.Load<Texture2D>(@textures[1]);
            backFaceTexture = modelManager.game.Content.Load<Texture2D>(@textures[2]);

        }

        public void LoadFaces(string frontTexture, string backTexture, string[] bodyTextures)
        {
            //textures is expected to have three textures: faces, top end, bottom end
            LoadFrontFace(frontTexture);
            LoadBackFace(backTexture);
            LoadBody(bodyTextures);
        }

        public void LoadFrontFace(string texture)
        {
            //textures is expected to have three textures: faces, top end, bottom end
            frontFaceTexture = modelManager.game.Content.Load<Texture2D>(@texture);
        }

        public void LoadBackFace(string texture)
        {
            //textures is expected to have three textures: faces, top end, bottom end
            backFaceTexture = modelManager.game.Content.Load<Texture2D>(@texture);
        }

        public void LoadBody(string[] textures)
        {
            //textures is expected to have three textures: faces, top end, bottom end
            for (int i = 0; i < numberOfFaces; i++)
                bodyTextures[i] = modelManager.game.Content.Load<Texture2D>(@textures[i]);
        }

        public override void Update(GameTime gameTime)
        {

            world = dimensionScaleMatrix;
            world *= orientationMatrix;
            world *= orientTranslationMatrix;

            world *= worldSpinYMatrix * worldSpinXMatrix * worldTranslationMatrix * worldOrbitMatrix;
            base.Update(gameTime);
        }

        public BoundingBox getBoundingBox()
        {
            return BoundingBox.CreateFromSphere(getBoundingSphere());
        }

        public BoundingSphere getBoundingSphere()
        {
            Vector3 position = Matrix.Invert(getWorld()).Translation;
            return new BoundingSphere(position, nominal_radius);
        }

        public Vector3 getPosition()
        {
            return Matrix.Invert(getWorld()).Translation;
        }

        public override void Draw(BasicEffect effect)
        {

            effect.World = world; //set position to draw model

            effect.VertexColorEnabled = false;  //because we are about to render textures not color
            effect.TextureEnabled = true;  //because we are about to render textures not colors


            for (int i = 0; i < numberOfFaces; i++)
            {
                effect.Texture = bodyTextures[i];
                effect.CurrentTechnique.Passes[0].Apply();
                modelManager.game.GraphicsDevice.DrawUserPrimitives<VertexPositionTexture>(PrimitiveType.TriangleList, vertexTextures, i * 3 * 2, 2);
            //    game.GraphicsDevice.DrawUserPrimitives<VertexPositionTexture>(PrimitiveType.TriangleList, vertexTopEndTextures, i * 3, 1);

            }
            
            for (int i = 0; i < numberOfFaces; i++)
            {
                effect.Texture = frontFaceTexture;
                effect.CurrentTechnique.Passes[0].Apply();
                modelManager.game.GraphicsDevice.DrawUserPrimitives<VertexPositionTexture>(PrimitiveType.TriangleList, vertexTopEndTextures, i * 3, 1);

            }
            for (int i = 0; i < numberOfFaces; i++)
            {
                effect.Texture = backFaceTexture;
                effect.CurrentTechnique.Passes[0].Apply();
                modelManager.game.GraphicsDevice.DrawUserPrimitives<VertexPositionTexture>(PrimitiveType.TriangleList, vertexBottomEndTextures, i * 3, 1);

            }
            

        }

    }
}
