﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Fellowship
{
    public abstract class Camera: Microsoft.Xna.Framework.GameComponent
    {

        //view and projection matrices of the camera
        public Matrix view { get; protected set; }
        public Matrix projection { get; protected set; }

        //these vectors will be used to recreate the camera view matrix each frame
        public Vector3 cameraPosition { get; protected set; }
        protected Vector3 cameraDirection; //direction vector, not a target point, cannot be 0,0,0
        protected Vector3 cameraUp;

        protected float sideWaysSpeed; //speed of camera sideways movement
        protected float upDownSpeed; //speed of camera up and down movement
        protected float cameraTiltSpeed; //speed of camera tilt (Pitch) rotation
        protected float cameraPanSpeed; //speed of camera pan (Yaw) rotation
        protected float cameraRollSpeed; //speed of camera Roll rotation

        public bool parallaxEnabled { get; set; }

        public Viewport viewport { get; protected set; }

        public Camera(Game1 game)
            : base(game)
        {
            cameraPosition = Vector3.Zero;
            cameraUp = Vector3.Zero;
            cameraDirection = Vector3.Zero;

            viewport = game.GraphicsDevice.Viewport;
        }

        public Camera(Game1 game, Vector3 pos, Vector3 target, Vector3 up)
            : base(game)
        {
            cameraPosition = pos;
            cameraUp = up;
            cameraDirection = target - pos;
            cameraDirection.Normalize();

            viewport = game.GraphicsDevice.Viewport;
        }

        protected virtual void CreateLookAt()
        {
            //Create a LookAt view matrix using the camera direction
            //This will turn the camera direction and position into a target point
            view = Matrix.CreateLookAt(cameraPosition, cameraPosition + cameraDirection, cameraUp);
        }

    }
}
