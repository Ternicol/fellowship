﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Fellowship
{
    class LavaAbility : LimitedAbility
    {

        public LavaAbility(Game game)
        {
            icon = game.Content.Load<Texture2D>(@"Icons/LavaIcon");
        }
        public override void Launch(Adventurer adv)
        {
            if (available())
            {
                NUMBER_OF_USES--;
                adv.lavaAbility = true;
            }

        }

    }
}