﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Fellowship
{
    public abstract class Level: Microsoft.Xna.Framework.DrawableGameComponent
    {
        public Game game;
        protected TimeSpan timer;

        public List<TerrainBlock> groundBlocks = new List<TerrainBlock>();
        public ActivatorArea endArea;

        internal List<Adventurer> adventurers = new List<Adventurer>();
        internal Adventurer activeAdventurer;

        internal Terrain terrain;

        protected List<Background> layers = new List<Background>();

        internal BasicEffect effect;

        internal Song music;
        internal SoundEffect ambiant;
        internal SoundEffectInstance ambiantInstance;

        internal int unitToSpawn = 0;
        internal TimeSpan spawnCounter = new TimeSpan(0, 0, 0);
        internal List<int> spawnTimes = new List<int>();

        public int numberOfAdventurers = 0;
        public int score = 0;

        public bool failed = false;
        public bool succeeded = false;

        public bool outOfTime = false;

        public bool levelMusicLaunched = false;
       
        public Level(Game game)
            : base(game)
        {
            // TODO: Construct any child components here
        }


        private void UpdateBackgrounds(GameTime gameTime)
        {
            Vector3 parallax = new Vector3(((Game1)Game).camera.cameraPosition.X*0.1f, 0, 0);

            if (Keyboard.GetState().IsKeyDown(Keys.D) || Keyboard.GetState().IsKeyDown(Keys.A))
            {
                layers[0].translate(Matrix.CreateTranslation(
                    (parallax.X - layers[0].getWorldTranslation().Translation.X),
                    parallax.Y,
                    parallax.Z));
                layers[1].translate(Matrix.CreateTranslation(
                    (-parallax.X - layers[1].getWorldTranslation().Translation.X) * 0.0001f,
                    parallax.Y,
                    parallax.Z));
                layers[2].translate(Matrix.CreateTranslation(
                    (parallax.X - layers[2].getWorldTranslation().Translation.X) * 0.01f,
                    parallax.Y,
                    parallax.Z));
                layers[3].translate(Matrix.CreateTranslation(
                    (-parallax.X - layers[3].getWorldTranslation().Translation.X) * 0.5f,
                    parallax.Y,
                    parallax.Z));
                layers[4].translate(Matrix.CreateTranslation(
                    (parallax.X - layers[4].getWorldTranslation().Translation.X) * 0.1f,
                    parallax.Y,
                    parallax.Z));
            }


            for (int i = 0; i < layers.Count; i++)
                layers[i].Update(gameTime);

        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            // TODO: Add your update code here
            UpdateBackgrounds(gameTime);
            endArea.texture.Update(gameTime);

            for (int i = 0; i < groundBlocks.Count; i++)
            {
                groundBlocks[i].Update(gameTime);
            }


            foreach (Adventurer adv in adventurers)
            {
                if (adv.readyToSpawn && !adv.isDead && !adv.reachedEnd)
                    adv.Update(groundBlocks);
                if ((!adv.reachedEnd && endArea.getBoundingBox().Contains(adv.getBoundingBox())
                    == ContainmentType.Contains))
                {
                    adv.reachedEnd = true;
                    adv.footstepEffectGrassInstance.Dispose();
                    adv.footstepEffectStoneInstance.Dispose();
                    adv.swimmingEffectWaterInstance.Dispose();
                    numberOfAdventurers--;
                    score += 50;
                }
                if (adv.isDead)
                {
                    adv.footstepEffectGrassInstance.Dispose();
                    adv.footstepEffectStoneInstance.Dispose();
                    adv.swimmingEffectWaterInstance.Dispose();
                }
            }

            UpdateTimer(gameTime);

            if (Finished() || outOfTime)
            {
                if (FinishedSuccess())
                    succeeded = true;
                else
                    failed = true;
            }

            base.Update(gameTime);
        }

        public bool Finished()
        {
            foreach (Adventurer adv in adventurers)
            {
                if (!adv.isDead && !adv.reachedEnd)
                    return false;
            }
            return true;
        }

        public bool FinishedSuccess()
        {
            int counter = 0;
            int num_to_success = (int)Math.Round((double)adventurers.Count * 2 / 3);

            foreach (Adventurer adv in adventurers)
            {
                foreach (TerrainBlock block in groundBlocks)
                {
                    if (!adv.reachedEnd && !adv.isDead && !block.isDealtWith() &&
                        block is EnemyObstacle && block.getBoundingBox().Contains(adv.getPosition()) == ContainmentType.Contains)
                    {
                        block.interact(adv);

                    }
                    if ((!adv.reachedEnd && !adv.isDead && !block.isDealtWith() &&
                        block is SnareObstacle && block.getBoundingBox().Contains(adv.getBoundingBox().GetCorners()[0]) == ContainmentType.Contains
                        && block.getBoundingBox().Contains(adv.getBoundingBox().GetCorners()[1]) == ContainmentType.Contains))
                    {
                        block.interact(adv);
                    }
                }

                if (!adv.isDead && !adv.reachedEnd)
                    return false;
                if(adv.reachedEnd)
                    counter++;
            }            if(counter >= num_to_success)
                return true;
            return false;
        }

        private void UpdateTimer(GameTime gameTime)
        {
            if (timer.Minutes == 0 && timer.Seconds == 0)
                outOfTime = true;

            timer -= gameTime.ElapsedGameTime;

            if (spawnCounter.Seconds < spawnTimes.Count * 5 + 1)
            {
                spawnCounter += gameTime.ElapsedGameTime;
                for (int i = 0; i < spawnTimes.Count; i++)
                {
                    if (spawnCounter.Seconds == spawnTimes.ElementAt(i))
                        adventurers.ElementAt(i).readyToSpawn = true;
                }
            }

        }

        private void DrawBackgrounds(GameTime gameTime)
        {

            Game1 g = ((Game1)Game);

            for (int i = 0; i < layers.Count; i++)
                layers[i].Draw(g.effect);


        }

        public override void Draw(GameTime gameTime)
        {
            GraphicsDevice.BlendState = BlendState.AlphaBlend;
            GraphicsDevice.DepthStencilState = DepthStencilState.Default;

            DrawBackgrounds(gameTime);


            Game1 g = ((Game1)Game);

            endArea.texture.Draw(g.effect);

            for (int i = 0; i < groundBlocks.Count; i++)
            {
                if(!groundBlocks[i].isDealtWith())
                    groundBlocks[i].Draw(g.effect);
                if (groundBlocks[i] is SnareObstacle)
                    groundBlocks[i].Draw(g.effect);
            }

            foreach (Adventurer adv in adventurers)
            {
                if (adv.readyToSpawn && !adv.isDead && !adv.reachedEnd)
                    adv.Draw(g.camera);
            }

            DrawScoreData(gameTime);
            DrawTimer(gameTime);

            base.Draw(gameTime);
        }

        public void DrawScoreData(GameTime gameTime)
        {
            Vector3 block1Pos = adventurers.ElementAt(0).getPosition();

            String adventurersData = "Adventurers: " + numberOfAdventurers;
            String scoreData = "Score: " + score;


            ((Game1)Game).spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, RasterizerState.CullNone);
            ((Game1)Game).spriteBatch.DrawString(((Game1)Game).datafont, adventurersData,
                new Vector2(20, 10), Color.Red);
            ((Game1)Game).spriteBatch.DrawString(((Game1)Game).datafont, scoreData,
                new Vector2(20, 40), Color.Red);
            ((Game1)Game).spriteBatch.End();
        }

        public void DrawTimer(GameTime gameTime)
        {
            String timerString = timer.Minutes.ToString("00") + ":" + timer.Seconds.ToString("00");
            ((Game1)Game).spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, RasterizerState.CullNone);
            ((Game1)Game).spriteBatch.DrawString(((Game1)Game).datafont, timerString,
                new Vector2(((Game1)Game).Window.ClientBounds.Width - 150, 10), Color.Red);
            ((Game1)Game).spriteBatch.End();
        }

        public string getTime()
        {
            return timer.Minutes.ToString("00") + ":" + timer.Seconds.ToString("00");
        }

        public void turnIntoStone(LavaBlock block)
        {
            int i = 0;
            foreach (TerrainBlock m in groundBlocks)
            {
                if (block == m)
                {
                    groundBlocks[i] = terrain.turnIntoStone(block);
                    break;
                }
                i++;
            }
        }

    }
}
