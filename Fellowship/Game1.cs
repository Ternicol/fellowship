using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Fellowship
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        internal GraphicsDeviceManager graphics;
        internal SpriteBatch spriteBatch;

        internal SpriteFont datafont;

        //Camera section
        internal Camera camera;
        Vector3 cameraInitialPosition;

        //Effect section
        internal BasicEffect effect;

        //World matrix section
        internal Matrix world;

        //Support matrix section
        Matrix worldSpinYMatrix; //rotation about Y axis
        Matrix worldSpinXMatrix; //rotation about X axis
        Matrix worldOrbitMatrix;
        Matrix worldTranslationMatrix;
        Matrix worldScaleMatrix;

        internal Level modelManager;
        AbilityUI abilityUI;
        AdventurerUI adventurerUI;

        Texture2D startScreen, startScreen1, bLevelScreen, bLevelScreen1;
        Texture2D bLevelScreenFailed, bLevelScreenFailed1;
        SpriteFont knightFont;
        bool startScreenShown, bLevelScreenShown, newGameSelected, nextLevelSelected;

        bool level1Finished = false, level2Finished = false;

        internal Song menuMusic;
        internal SoundEffect menuSelect;
        internal SoundEffectInstance menuSelectInstance;

        internal int current_level_num = 1;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            
            graphics.PreferredBackBufferWidth = 1280;
            graphics.PreferredBackBufferHeight = 800;
        //    graphics.PreferMultiSampling = true;
            IsMouseVisible = true;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            LaunchLevel(current_level_num);

            world = Matrix.Identity;

            worldTranslationMatrix = Matrix.Identity;
            worldSpinYMatrix = Matrix.Identity;
            worldSpinXMatrix = Matrix.Identity;
            worldOrbitMatrix = Matrix.Identity;
            worldScaleMatrix = Matrix.Identity;

            startScreenShown = true;
            bLevelScreenShown = false;
            newGameSelected = true;
            nextLevelSelected = true; 

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            datafont = Content.Load<SpriteFont>(@"Fonts/DataFont");

            startScreen = Content.Load<Texture2D>(@"Textures/StartScreen");
            startScreen1 = Content.Load<Texture2D>(@"Textures/StartScreen1");
            bLevelScreen = Content.Load<Texture2D>(@"Textures/bLevelScreen");
            bLevelScreen1 = Content.Load<Texture2D>(@"Textures/bLevelScreen1");
            bLevelScreenFailed = Content.Load<Texture2D>(@"Textures/bLevelScreenFailed");
            bLevelScreenFailed1 = Content.Load<Texture2D>(@"Textures/bLevelScreenFailed1");
            knightFont = Content.Load<SpriteFont>(@"Fonts/knightsquest");

            menuMusic = Content.Load<Song>("Music/menu_music");
            MediaPlayer.Volume = 0.5f;
            MediaPlayer.IsRepeating = true;
            MediaPlayer.Play(menuMusic);

            menuSelect = Content.Load<SoundEffect>(@"Sounds/Head Hitting Metal");
            effect = new BasicEffect(GraphicsDevice);
            
            RasterizerState rs = new RasterizerState();
            rs.CullMode = CullMode.None;
            GraphicsDevice.RasterizerState = rs;
        }

        private void ReloadLevel(int level_num)
        {
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            KeyboardState keyboardState = Keyboard.GetState();

            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            if (keyboardState.IsKeyDown(Keys.Escape))
                this.Exit();

            if (modelManager.succeeded || modelManager.failed)
                bLevelScreenShown = true;

            if (startScreenShown == true)
                updateStartScreen();

            if (bLevelScreenShown)
                updateLevelScreen();

            if (!startScreenShown && !bLevelScreenShown)
            {
                if (!modelManager.levelMusicLaunched)
                {
                    MediaPlayer.Play(modelManager.music);
                    modelManager.levelMusicLaunched = true;
                }
                base.Update(gameTime);
            }
        }
        
        protected void updateStartScreen()
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Enter) && newGameSelected)
            {
                startScreenShown = false;
                LaunchLevel(current_level_num);
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Enter) && !newGameSelected)
            {
                startScreenShown = false;
                TextReader tr = new StreamReader(@"Content\LevelMaps\saveFile.txt");
                int l = int.Parse(tr.ReadLine());
                tr.Close();
                try
                {
                    current_level_num = l;
                    LaunchLevel(current_level_num);
                }
                catch (Exception e)
                {
                    current_level_num = 1;
                    LaunchLevel(current_level_num);
                }
            }

            if (Keyboard.GetState().IsKeyDown(Keys.Down) && newGameSelected)
            {
                newGameSelected = false;
                menuSelectInstance = menuSelect.CreateInstance();
                menuSelectInstance.Play();
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Up) && !newGameSelected)
            {
                newGameSelected = true;
                menuSelectInstance = menuSelect.CreateInstance();
                menuSelectInstance.Play();
            }
        }

        public void levelScreenOn() { bLevelScreenShown = true; }

        protected void updateLevelScreen()
        {
            
            if (Keyboard.GetState().IsKeyDown(Keys.Enter) && nextLevelSelected)
            {
                if(current_level_num < 5)
                {
                    LaunchLevel(modelManager.failed? current_level_num: ++current_level_num);
                    bLevelScreenShown = false;
                }
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Enter) && !nextLevelSelected)
            {
                bLevelScreenShown = false;
                TextWriter tw = new StreamWriter(@"Content\LevelMaps\saveFile.txt");
                tw.WriteLine(current_level_num);
                tw.Close();
                this.Exit();
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Down) && nextLevelSelected)
            {
                nextLevelSelected = false;
                menuSelectInstance = menuSelect.CreateInstance();
                menuSelectInstance.Play();
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Up) && !nextLevelSelected)
            {
                nextLevelSelected = true;
                menuSelectInstance = menuSelect.CreateInstance();
                menuSelectInstance.Play();
            }
        }

        private void LaunchLevel(int level_num)
        {
            if (Components.Contains(abilityUI))
                Components.Remove(abilityUI);
            if (Components.Contains(adventurerUI))
                Components.Remove(adventurerUI);
            if (Components.Contains(modelManager))
                Components.Remove(modelManager);

            cameraInitialPosition = new Vector3(-6900, 0, 8000);
            Vector3 target = new Vector3(
                graphics.PreferredBackBufferWidth / 2,
                graphics.PreferredBackBufferHeight / 2, 0.0f);

            camera = new Camera2D(this,  //game
                                    cameraInitialPosition,  //camera location
                                    target,    //target camera is looking at -origin (0,0,0)
                                    Vector3.Up  //orientation (0,1,0)
                                    );

            Components.Add(camera);

            switch(level_num)
            {
                case 1: modelManager = new Level1(this); break;
                case 2: modelManager = new Level2(this); break;
                case 3: modelManager = new Level3(this); break;
                case 4: modelManager = new Level4(this); break;
            }

            Components.Add(modelManager); //add game component to game loop

            adventurerUI = new AdventurerUI(this);
            Components.Add(adventurerUI);

            abilityUI = new AbilityUI(this);
            Components.Add(abilityUI);
        }
        
        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            GraphicsDevice.DepthStencilState = DepthStencilState.Default;

            // TODO: Add your drawing code here
            

            //set camera info and object info
            effect.World = world;  //position in world to draw object
            //effect.World = worldScaleMatrix * world;  //scale and position in world to draw object
            effect.View = camera.view; //use camera view frustrum
            effect.Projection = camera.projection; //use camera projection

            effect.VertexColorEnabled = true;

            if (startScreenShown)
                drawStartScreen();
            if (bLevelScreenShown)
            {
                if(modelManager.succeeded)
                    drawLevelScreenSuccess();
                else
                    drawLevelScreenFail();
            }

            if (!startScreenShown && !bLevelScreenShown)
                base.Draw(gameTime);
        }

        protected void drawStartScreen()
        {
            spriteBatch.Begin(); 
            if (newGameSelected)
                spriteBatch.Draw(startScreen, Vector2.Zero, Color.White);
            else
                spriteBatch.Draw(startScreen1, Vector2.Zero, Color.White);
            spriteBatch.End(); 
        }
        
        protected void drawLevelScreenSuccess()
        {
            spriteBatch.Begin(); 
            if (nextLevelSelected)
                spriteBatch.Draw(bLevelScreen, Vector2.Zero, Color.White);
            else
                spriteBatch.Draw(bLevelScreen1, Vector2.Zero, Color.White);
            spriteBatch.DrawString(knightFont, "Adventurers Left: " + modelManager.numberOfAdventurers, new Vector2(390, 250), Color.White);
            spriteBatch.DrawString(knightFont, "Completion Time: " + modelManager.getTime(), new Vector2(390, 300), Color.White);
            spriteBatch.DrawString(knightFont, "Total Score: " + modelManager.score, new Vector2(390, 350), Color.White);
            spriteBatch.End(); 
        }

        protected void drawLevelScreenFail()
        {
            spriteBatch.Begin();
            if (nextLevelSelected)
                spriteBatch.Draw(bLevelScreenFailed, Vector2.Zero, Color.White);
            else
                spriteBatch.Draw(bLevelScreenFailed1, Vector2.Zero, Color.White);
            spriteBatch.DrawString(knightFont, "Adventurers Alive: " + modelManager.numberOfAdventurers, new Vector2(390, 250), Color.White);
            spriteBatch.DrawString(knightFont, "Completion Time: " + modelManager.getTime(), new Vector2(390, 300), Color.White);
            spriteBatch.DrawString(knightFont, "Total Score: " + modelManager.score, new Vector2(390, 350), Color.White);
            spriteBatch.End();
        }
        
        
    }
}
