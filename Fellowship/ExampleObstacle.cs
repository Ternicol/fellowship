﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Fellowship
{
    class ExampleObstacle :Obstacle
    {
        Vector3 position;
        CubeModel model;
        bool withAdv;
        bool withArcher;
        bool withWarrior;
        bool withShamen;
        bool withMage;
        bool withEngi;
        bool withWarrEngi;
        bool withWarrArch;
        bool withWarSham;
        bool withWarrMage;
        bool withArchMage;
        bool withArchSham;
        bool withArchEng;
        bool withMageSham;
        bool withMageEng;
        bool withShamEng;

        public ExampleObstacle(Vector3 p, CubeModel m)
        {//basic constructor
            position = p;
            model = m;
            withAdv = true;//shows what it can interact with
            withArchEng = withArcher = withArchMage = withArchSham = withEngi = withMage = withMageEng = withShamen = withShamEng = withWarrArch = withWarrEngi = withWarrior = withWarrMage = withWarSham = false;
        }
        public void InteractWAdv(Adventurer adv)//Function to deal with the adventurer class
        {
            if (position.X == adv.getPosition().X)
            {
                adv.nominalSpeed.X = 0;
            }
        }//Dummy functions to appease the compiler
        public void InteractWArc(Adventurer adv) { }
        public void InteractWWar(Adventurer adv) { }
        public void InteractWSha(Adventurer adv) { }
        public void InteractWMag(Adventurer adv) { }
        public void InteractWEng(Adventurer adv) { }
        public void InteractWWarArr(Adventurer adv) { }
        public void InteractWWarMag(Adventurer adv) { }
        public void InteractWWarSham(Adventurer adv) { }
        public void InteractWWarEng(Adventurer adv) { }
        public void InteractWArrSha(Adventurer adv) { }
        public void InteractWArrMag(Adventurer adv) { }
        public void InteractWArrEng(Adventurer adv) { }
        public void InteractWShaMag(Adventurer adv) { }
        public void InteractWShaEng(Adventurer adv) { }
        public void InteractWEngMag(Adventurer adv) { }
    }
}
