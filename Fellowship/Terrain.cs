﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Fellowship
{
    class Terrain           //This class creates, holds, and initalizes the levels. Currently does so by reading a text file. 
    {
        String mapPath;
        int mapSize_x, mapSize_y;
        int pos_x, pos_y, pos_z;
        int blockSize;
        Level level;
        TerrainBlock[,] terrain;
        char[,] terrainGuide;
        char[,] obstacleGuide;

        public Terrain(String fileName, int x, int y, Level l)
        {
            mapPath = fileName;
            mapSize_x = x;
            mapSize_y = y;
            level = l;
            terrain = new TerrainBlock[mapSize_x, mapSize_y];
            terrainGuide = new char[mapSize_x, mapSize_y];
            TextReader tr = new StreamReader(mapPath);  // This has to be changed to match where the text file is. 

            for (int i = 0; i < mapSize_x; i++)
            {
                String line = tr.ReadLine();

                for (int p = 0; p < mapSize_y; p++)
                {
                    terrainGuide[i, p] = line[p];
                }
            }

            tr.Close();

        }

        public void initializeTerrain(int x, int y, int z, int b)
        {

            pos_x = x;
            pos_y = y;
            pos_z = z;
            blockSize = b;
            for (int i = 0; i < mapSize_x; i++)
            {
                for (int p = 0; p < mapSize_y; p++)
                {
                    switch (terrainGuide[i, p])
                    {
                        case 'L':
                            terrain[i, p] = new LavaBlock(level, blockSize, blockSize, blockSize);
                            terrain[i, p].translate(Matrix.CreateTranslation(pos_x + (p * blockSize), pos_y - (i * blockSize), pos_z));
                            break;
                        case 'G':
                            terrain[i, p] = new GrassBlock(level, blockSize, blockSize, blockSize);
                            terrain[i, p].translate(Matrix.CreateTranslation(pos_x + (p * blockSize), pos_y - (i * blockSize), pos_z));
                            break;
                        case 'S':
                            terrain[i, p] = new StoneBlock(level, blockSize, blockSize, blockSize);
                            terrain[i, p].translate(Matrix.CreateTranslation(pos_x + (p * blockSize), pos_y - (i * blockSize), pos_z));
                            break;
                        case 'W':
                            terrain[i, p] = new WaterBlock(level, blockSize, blockSize, blockSize);
                            terrain[i, p].translate(Matrix.CreateTranslation(pos_x + (p * blockSize), pos_y - (i * blockSize), pos_z));
                            break;
                        case 'H':
                            terrain[i, p] = new SnareObstacle(level, blockSize, blockSize, blockSize);
                            terrain[i, p].translate(Matrix.CreateTranslation(pos_x + (p * blockSize), pos_y - (i * blockSize), pos_z));
                            break;
                        case 'E':
                            terrain[i, p] = new EnemyObstacle(level, blockSize, blockSize, blockSize);
                            terrain[i, p].translate(Matrix.CreateTranslation(pos_x + (p * blockSize), pos_y - (i * blockSize), pos_z));
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        public void initializeTextures()
        {

            for (int i = 0; i < mapSize_x; i++)
            {
                for (int p = 0; p < mapSize_y; p++)
                {
                    if (terrainGuide[i, p] != '0') { terrain[i, p].LoadContent(); }
                }
            }
        }

        public int getSizeX() { return mapSize_x; }
        public int getSizeY() { return mapSize_y; }
        public TerrainBlock getBlock(int i, int p) { return terrain[i, p]; }
        public char getBlockGuide(int i, int p) { return terrainGuide[i, p]; }

        public TerrainBlock turnIntoStone(TerrainBlock block)
        {
            for (int i = 0; i < mapSize_x; i++)
            {
                for (int p = 0; p < mapSize_y; p++)
                {
                    if (terrain[i, p] == block)
                    {
                        terrain[i, p] = new StoneBlock(level, blockSize, blockSize, blockSize);
                        terrain[i, p].translate(Matrix.CreateTranslation(pos_x + (p * blockSize), pos_y - (i * blockSize), pos_z));
                        terrain[i, p].LoadContent();
                        return terrain[i, p]; 
                    }
                }
            }
            return block; 
        }
    }
}
