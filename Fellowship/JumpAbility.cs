﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Fellowship
{
    class JumpAbility: LimitedAbility
    {

        public JumpAbility(Game game)
        {
            icon = game.Content.Load<Texture2D>(@"Icons/2");
        }

        public override void Launch(Adventurer adv)
        {
            if (available())
            {
                NUMBER_OF_USES--;

                if (!adv.isJumping)
                {
                    adv.isJumping = true;
                    adv.nominalSpeed.Y = 40.0f;
                    adv.acceleration.Y = 0;
                }
            }
           
        }

    }
}
