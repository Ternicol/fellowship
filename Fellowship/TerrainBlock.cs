﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Fellowship
{
    public class TerrainBlock : PrimitiveModel
    {
        protected Texture2D frontFace;
        protected Texture2D backFace;
        protected Texture2D topFace;
        protected Texture2D bottomFace;
        protected Texture2D rightFace;
        protected Texture2D leftFace;
        public Boolean dealtWith;

        float nominal_width;
        float nominal_height;
        float nominal_depth;

        internal float width;
        internal float height;
        internal float depth;

        public String textureName;

        VertexPositionTexture[] vertexTextures; //vertices of triangles with texture mapping





        public TerrainBlock(Level modelManager, float w, float h, float d)
            : base(modelManager)
        {
            width = w;
            height = h;
            depth = d;
            dealtWith = false;
            initializeCube();
            dimensionScaleMatrix = Matrix.CreateScale(w / nominal_width, h / nominal_height, d / nominal_depth);
        }

        public TerrainBlock(Level modelManager, float w, float h, float d, float x, float y, float z)
            : base(modelManager)
        {
            width = w;
            height = h;
            depth = d;

            dealtWith = false;
            initializeCube();
            dimensionScaleMatrix = Matrix.CreateScale(w / nominal_width, h / nominal_height, d / nominal_depth);
            worldTranslationMatrix = Matrix.CreateTranslation(x, y, z);
        }

        public TerrainBlock(Level modelManager)
            : base(modelManager)
        {

            initializeCube();
        }

        public Vector3 getForwardDirectionVector()
        {
            Matrix directionMatrix = orientationMatrix * worldSpinXMatrix * worldSpinYMatrix;
            return Vector3.Transform(Vector3.UnitX, directionMatrix);
        }





        private void initializeCube()
        {


            //These are the vertices of a die
            //each of the 6 faces is represented by a quad of two triangles
            //stored in a triangle list format
            dealtWith = false;
            //nominal 2x2x2 cube
            dimensionScaleMatrix = Matrix.Identity;
            orientationMatrix = Matrix.Identity;
            orientTranslationMatrix = Matrix.Identity;


            nominal_width = 2.0f;
            nominal_height = 2.0f;
            nominal_depth = 2.0f;

            vertexTextures = new VertexPositionTexture[6 * 2 * 3];

            //front face 6
            vertexTextures[0] = new VertexPositionTexture(new Vector3(-1, 1, 1), new Vector2(0, 0));
            vertexTextures[1] = new VertexPositionTexture(new Vector3(1, 1, 1), new Vector2(1, 0));
            vertexTextures[2] = new VertexPositionTexture(new Vector3(-1, -1, 1), new Vector2(0, 1));
            vertexTextures[3] = new VertexPositionTexture(new Vector3(1, 1, 1), new Vector2(1, 0));
            vertexTextures[4] = new VertexPositionTexture(new Vector3(1, -1, 1), new Vector2(1, 1));
            vertexTextures[5] = new VertexPositionTexture(new Vector3(-1, -1, 1), new Vector2(0, 1));

            //back face 1
            vertexTextures[6] = new VertexPositionTexture(new Vector3(-1, 1, -1), new Vector2(0, 0));
            vertexTextures[7] = new VertexPositionTexture(new Vector3(-1, -1, -1), new Vector2(0, 1));
            vertexTextures[8] = new VertexPositionTexture(new Vector3(1, 1, -1), new Vector2(1, 0));
            vertexTextures[9] = new VertexPositionTexture(new Vector3(1, 1, -1), new Vector2(1, 0));
            vertexTextures[10] = new VertexPositionTexture(new Vector3(-1, -1, -1), new Vector2(0, 1));
            vertexTextures[11] = new VertexPositionTexture(new Vector3(1, -1, -1), new Vector2(1, 1));


            //top face 4
            vertexTextures[12] = new VertexPositionTexture(new Vector3(-1, 1, 1), new Vector2(0, 1)); //0
            vertexTextures[13] = new VertexPositionTexture(new Vector3(-1, 1, -1), new Vector2(0, 0)); //4
            vertexTextures[14] = new VertexPositionTexture(new Vector3(1, 1, -1), new Vector2(1, 0)); //5
            vertexTextures[15] = new VertexPositionTexture(new Vector3(-1, 1, 1), new Vector2(0, 1)); //0
            vertexTextures[16] = new VertexPositionTexture(new Vector3(1, 1, -1), new Vector2(1, 0)); //5
            vertexTextures[17] = new VertexPositionTexture(new Vector3(1, 1, 1), new Vector2(1, 1));  //1

            //bottom face 3
            vertexTextures[18] = new VertexPositionTexture(new Vector3(-1, -1, 1), new Vector2(0, 1)); //3
            vertexTextures[19] = new VertexPositionTexture(new Vector3(1, -1, -1), new Vector2(1, 0)); //6
            vertexTextures[20] = new VertexPositionTexture(new Vector3(-1, -1, -1), new Vector2(0, 0)); //7
            vertexTextures[21] = new VertexPositionTexture(new Vector3(-1, -1, 1), new Vector2(0, 1)); //3
            vertexTextures[22] = new VertexPositionTexture(new Vector3(1, -1, 1), new Vector2(1, 1)); //2
            vertexTextures[23] = new VertexPositionTexture(new Vector3(1, -1, -1), new Vector2(1, 0));  //6

            //right face 5
            vertexTextures[24] = new VertexPositionTexture(new Vector3(1, 1, 1), new Vector2(0, 0)); //1
            vertexTextures[25] = new VertexPositionTexture(new Vector3(1, 1, -1), new Vector2(1, 0)); //5
            vertexTextures[26] = new VertexPositionTexture(new Vector3(1, -1, 1), new Vector2(0, 1)); //2
            vertexTextures[27] = new VertexPositionTexture(new Vector3(1, -1, 1), new Vector2(0, 1)); //2
            vertexTextures[28] = new VertexPositionTexture(new Vector3(1, 1, -1), new Vector2(1, 0)); //5
            vertexTextures[29] = new VertexPositionTexture(new Vector3(1, -1, -1), new Vector2(1, 1));  //6

            //left face 2
            vertexTextures[30] = new VertexPositionTexture(new Vector3(-1, -1, -1), new Vector2(0, 1)); //7
            vertexTextures[31] = new VertexPositionTexture(new Vector3(-1, 1, -1), new Vector2(0, 0)); //4
            vertexTextures[32] = new VertexPositionTexture(new Vector3(-1, 1, 1), new Vector2(1, 0)); //0
            vertexTextures[33] = new VertexPositionTexture(new Vector3(-1, -1, -1), new Vector2(0, 1)); //7
            vertexTextures[34] = new VertexPositionTexture(new Vector3(-1, 1, 1), new Vector2(1, 0)); //0
            vertexTextures[35] = new VertexPositionTexture(new Vector3(-1, -1, 1), new Vector2(1, 1));  //3


            indices = new short[]
            {
                0, 1, 2, 3, 4, 5,
                6, 7, 8, 9, 10, 11,
                12, 13, 14, 15, 16,
                17, 18, 19, 20, 21,
                22, 23, 24, 25, 26,
                27, 28, 29, 30, 31,
                32, 33, 34, 35
            };

        }

        public Vector3 getPosition()
        {
            return Matrix.Invert(getWorldTranslation()).Translation;
        }

        public Vector3 getRotation()
        {
            return Matrix.Invert(getWorldRotation()).Translation;
        }


        public BoundingBox getBoundingBox()
        {
            Vector3 pos = getPosition();
            Vector3 angle = getRotation();

            Vector3[] points = 
            {
                new Vector3(pos.X - width/2, pos.Y - height/2, pos.Z - depth/2),
                new Vector3(pos.X + width/2, pos.Y - height/2, pos.Z - depth/2),
                new Vector3(pos.X - width/2, pos.Y + height/2, pos.Z - depth/2),
                new Vector3(pos.X + width/2, pos.Y + height/2, pos.Z - depth/2),

                new Vector3(pos.X - width/2, pos.Y - height/2, pos.Z + depth/2),
                new Vector3(pos.X + width/2, pos.Y - height/2, pos.Z + depth/2),
                new Vector3(pos.X - width/2, pos.Y + height/2, pos.Z + depth/2),
                new Vector3(pos.X + width/2, pos.Y + height/2, pos.Z + depth/2)
            };

            return BoundingBox.CreateFromPoints(points);
        }

        public BoundingSphere getBoundingSphere()
        {
            return new BoundingSphere(getPosition(), width);
        }


        public override void LoadContent(string[] textures)
        {
            frontFace = modelManager.game.Content.Load<Texture2D>(@textures[0]);
            backFace = modelManager.game.Content.Load<Texture2D>(@textures[1]);
            topFace = modelManager.game.Content.Load<Texture2D>(@textures[2]);
            bottomFace = modelManager.game.Content.Load<Texture2D>(@textures[3]);
            rightFace = modelManager.game.Content.Load<Texture2D>(@textures[4]);
            leftFace = modelManager.game.Content.Load<Texture2D>(@textures[5]);

            SetBuffers();
        }

        public void LoadContent(string texture)
        {
            frontFace = modelManager.game.Content.Load<Texture2D>(@texture);
            backFace = modelManager.game.Content.Load<Texture2D>(@texture);
            topFace = modelManager.game.Content.Load<Texture2D>(@texture);
            bottomFace = modelManager.game.Content.Load<Texture2D>(@texture);
            rightFace = modelManager.game.Content.Load<Texture2D>(@texture);
            leftFace = modelManager.game.Content.Load<Texture2D>(@texture);

            SetBuffers();
        }

        public override void LoadContent()
        {
            frontFace = modelManager.game.Content.Load<Texture2D>(@textureName);
            backFace = modelManager.game.Content.Load<Texture2D>(@textureName);
            topFace = modelManager.game.Content.Load<Texture2D>(@textureName);
            bottomFace = modelManager.game.Content.Load<Texture2D>(@textureName);
            rightFace = modelManager.game.Content.Load<Texture2D>(@textureName);
            leftFace = modelManager.game.Content.Load<Texture2D>(@textureName);

            SetBuffers();
        }

        public override void Update(GameTime gameTime)
        {

            world = dimensionScaleMatrix;
            world *= orientationMatrix;
            world *= orientTranslationMatrix;

            world *= worldSpinXMatrix * worldSpinYMatrix * worldSpinZMatrix * worldTranslationMatrix * worldOrbitMatrix;
            base.Update(gameTime);
        }

        protected void SetBuffers()
        {
            vertexBuffer = new VertexBuffer(
                modelManager.GraphicsDevice,
                typeof(VertexPositionTexture),
                vertexTextures.Length,
                BufferUsage.None
                );

            vertexBuffer.SetData<VertexPositionTexture>(vertexTextures);

            indexBuffer = new IndexBuffer(
                modelManager.GraphicsDevice,
                IndexElementSize.SixteenBits,
                sizeof(short) * indices.Length,
                BufferUsage.None
                );

            //    indexBuffer.SetData<short>(indices);
        }

        public override void Draw(BasicEffect effect)
        {

            effect.World = world; //set position to draw model

            effect.VertexColorEnabled = false;  //because we are about to render textures not color
            effect.TextureEnabled = true;  //because we are about to render textures not colors

            modelManager.GraphicsDevice.Indices = indexBuffer;
            modelManager.GraphicsDevice.SetVertexBuffer(vertexBuffer);

            effect.Texture = frontFace;
            effect.CurrentTechnique.Passes[0].Apply();
            modelManager.GraphicsDevice.DrawPrimitives(PrimitiveType.TriangleList, 0, 2);

            effect.Texture = backFace;
            effect.CurrentTechnique.Passes[0].Apply();
            modelManager.GraphicsDevice.DrawPrimitives(PrimitiveType.TriangleList, 6, 2);

            effect.Texture = topFace;
            effect.CurrentTechnique.Passes[0].Apply();
            modelManager.GraphicsDevice.DrawPrimitives(PrimitiveType.TriangleList, 12, 2);

            effect.Texture = bottomFace;
            effect.CurrentTechnique.Passes[0].Apply();
            modelManager.GraphicsDevice.DrawPrimitives(PrimitiveType.TriangleList, 18, 2);

            effect.Texture = rightFace;
            effect.CurrentTechnique.Passes[0].Apply();
            modelManager.GraphicsDevice.DrawPrimitives(PrimitiveType.TriangleList, 24, 2);

            effect.Texture = leftFace;
            effect.CurrentTechnique.Passes[0].Apply();
            modelManager.GraphicsDevice.DrawPrimitives(PrimitiveType.TriangleList, 30, 2);


        }
        public virtual void interact(Adventurer adv) { }
        public Boolean isDealtWith()
        {
            return dealtWith;
        }


    }

}
